import React from 'react';
import { cleanup, fireEvent, render } from '@testing-library/react';
import Context from '../src/Content';

it('renders the context with the text', () => {
  const { queryByText } = render(<Context />);

  expect(queryByText(/adventure cat/i)).toBeTruthy();
});

it('renders the context with the image', () => {
  const { queryByAltText } = render(<Context />);

  expect(queryByAltText(/adventure cat profile/i)).toBeTruthy();
});