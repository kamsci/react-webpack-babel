import React from 'react';
import { cleanup, fireEvent, render } from '@testing-library/react';
import Header from '../src/Header';

it('renders the header with the title', () => {
  const { queryByText } = render(<Header />);

  expect(queryByText(/ReactJS - Webpack - Babel/i)).toBeTruthy();
});
