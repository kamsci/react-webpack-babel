import React from 'react';
import { cleanup, fireEvent, render } from '@testing-library/react';
import Footer from '../src/Footer';

it('renders the footer with the content', () => {
  const { queryByText } = render(<Footer />);

  expect(queryByText(/boilerplate/i)).toBeTruthy();
});