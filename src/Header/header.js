import React from 'react';

function Header() {
  return (
    <div className="dlog-bar">
      <h1>ReactJS - Webpack - Babel</h1>
    </div>
  );
}

export default Header;
