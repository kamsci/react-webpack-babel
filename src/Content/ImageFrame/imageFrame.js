import React from 'react';
import adventureCat from './adventureCat.jpg';
import './imageFrame.css';

function ImageFrame() {
  return (
    <img className="circle-image" src={adventureCat} title="Adventure Cat" alt="Adventure Cat Profile" />
  );
}

export default ImageFrame;
