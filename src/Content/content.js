import React from 'react';
import ImageFrame from './ImageFrame';

function Content() {
  return (
    <div className="content-container">
      <h3>Adventure Cat</h3>
      <ImageFrame />
    </div>
  );
}

export default Content;
