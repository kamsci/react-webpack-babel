# ReactJS - Webpack - Babel

This is a boilerplate for a ReactJS app served with webpack and babel. This boiler plate will grow and adapt as I decide on more libraries, modules, and best practices that I belive deserve to be in a boiler plate. For example, v1.0.0 provides support for basic css stylesheets, but I am still deciding on my prefered way to add styles to React components.

I'll keep track of my boiler plate versions with git tags. 

####  Tag 1.0.0
  - ReactJS/React-DOM
  - Webpack/Webpack-dev-server
  - Babel
  - Jest/testing-library/react

#### Use Boilerplate with Tags
| Terminal Command |  |
| ------ | ------ |
| git clone <repo> | Clone repo in new app location |
| git checkout <tagVersion> | Checkout tag of version you want, ie. 1.0.0 |
| rm .git | Delete .git file to remove git history. Delete or update any other unwanted files |
| git commit | Make the initial commit in your new repo |
